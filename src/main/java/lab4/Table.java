/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package lab4;

/**
 *
 * @author informatics
 */
public class Table {
    private String[][] matrix = new String[3][3];
    {
        matrix[0][0] = "1";
        matrix[0][1] = "2";
        matrix[0][2] = "3";
        matrix[1][0] = "4";
        matrix[1][1] = "5";
        matrix[1][2] = "6";
        matrix[2][0] = "7";
        matrix[2][1] = "8";
        matrix[2][2] = "9";
    }
    private Player Player1;
    private Player Player2;
    private Player currentPlayer;
    
    public Table(Player Player1,Player Player2){
        this.Player1 = Player1;
        this.Player2 = Player2;
        this.currentPlayer = Player1;
    }
    public void setXO(int num){
        if("X".equals(currentPlayer.getSymbol())){
            if(num == 1){
            matrix[0][0] = currentPlayer.getSymbol();
        } else if (num == 2) {
            matrix[0][1] = currentPlayer.getSymbol();
        } else if (num == 3) {
            matrix[0][2] = currentPlayer.getSymbol();
        } else if (num == 4) {
            matrix[1][0] = currentPlayer.getSymbol();
        } else if (num == 5) {
            matrix[1][1] = currentPlayer.getSymbol();
        } else if (num == 6) {
            matrix[1][2] = currentPlayer.getSymbol();
        } else if (num == 7) {
            matrix[2][0] = currentPlayer.getSymbol();
        } else if (num == 8) {
            matrix[2][1] = currentPlayer.getSymbol();
        } else if (num == 9) {
            matrix[2][2] = currentPlayer.getSymbol();
        }
        }
        else if("O".equals(currentPlayer.getSymbol())){
           if(num == 1){
            matrix[0][0] = currentPlayer.getSymbol();
        } else if (num == 2) {
            matrix[0][1] = currentPlayer.getSymbol();
        } else if (num == 3) {
            matrix[0][2] = currentPlayer.getSymbol();
        } else if (num == 4) {
            matrix[1][0] = currentPlayer.getSymbol();
        } else if (num == 5) {
            matrix[1][1] = currentPlayer.getSymbol();
        } else if (num == 6) {
            matrix[1][2] = currentPlayer.getSymbol();
        } else if (num == 7) {
            matrix[2][0] = currentPlayer.getSymbol();
        } else if (num == 8) {
            matrix[2][1] = currentPlayer.getSymbol();
        } else if (num == 9) {
            matrix[2][2] = currentPlayer.getSymbol();
        }
        }
    }
    
    public String[][] getTable(){
        return matrix;
    }
    public Player getCurrentPlayer(){
        return currentPlayer;
    }
    public void switchPlayer(){
        if(currentPlayer == Player1){
            currentPlayer = Player2;
        }else{
            currentPlayer = Player1;
        }
    }
    public boolean checkWin(){
        //X_horizontal
        if(currentPlayer == Player1){
            if(matrix[0][0] == "X" && matrix[0][1] == "X" && matrix[0][2] == "X"){
                return true;
            }else if(matrix[1][0] == "X" && matrix[1][1] == "X" && matrix[1][2] == "X"){
                return true;
            }else if(matrix[2][0] == "X" && matrix[2][1] == "X" && matrix[2][2] == "X"){
                return true;
            }
            //X_vertical
            else if(matrix[0][0] == "X" && matrix[1][0] == "X" && matrix[2][0] == "X"){
                return true;
            }else if(matrix[0][1] == "X" && matrix[1][1] == "X" && matrix[2][1] == "X"){
                return true;
            }else if(matrix[0][2] == "X" && matrix[1][2] == "X" && matrix[2][2] == "X"){
                return true;
            }
            //X_diagonally
            else if(matrix[0][0] == "X" && matrix[1][1] == "X" && matrix[2][2] == "X"){
                return true;
            }else if(matrix[0][2] == "X" && matrix[1][1] == "X" && matrix[2][0] == "X"){
                return true;
            }
            }
            //O_horizontal
            if(currentPlayer == Player2){
            if(matrix[0][0] == "O" && matrix[0][1] == "O" && matrix[0][2] == "O"){
                return true;
            }else if(matrix[1][0] == "O" && matrix[1][1] == "O" && matrix[1][2] == "O"){
                return true;
            }else if(matrix[2][0] == "O" && matrix[2][1] == "O" && matrix[2][2] == "O"){
                return true;
                //O_vertical
            }else if(matrix[0][0] == "O" && matrix[1][0] == "O" && matrix[2][0] == "O"){
                return true;
            }else if(matrix[0][1] == "O" && matrix[1][1] == "O" && matrix[2][1] == "O"){
                return true;
            }else if(matrix[0][2] == "O" && matrix[1][2] == "O" && matrix[2][2] == "O"){
                return true;
                //O_diagonally
            }else if(matrix[0][0] == "O" && matrix[1][1] == "O" && matrix[2][2] == "O"){
                return true;
            }else if(matrix[0][2] == "O" && matrix[1][1] == "O" && matrix[2][0] == "O"){
                return true;
            }
            }
            return false;
    }
    public void checkDraw(){
        System.out.println("!!!Draw!!!");
    }
    
    public void isWin(){
        System.out.println(currentPlayer.getSymbol() + " !!!Winner!!!");
    }
}
