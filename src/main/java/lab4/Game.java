/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package lab4;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

/**
 *
 * @author informatics
 */
public class Game {
    static Set<Integer> chosenNumbers = new HashSet<>();
    private Table table;
    private Player Player1,Player2;
    private int turnCount = 0;
    private boolean gameOver = false;
    private int num = 0;
    
    public Game(){
        Player1 = new Player("X");
        Player2 = new Player("O");
        table = new Table(Player1, Player2);
    }
    public void play(){
        showWelcome();
        newGame();
        while(true){
            gameOver = false;
            showTable();
            showTurn();
            inputNumber();
            table.setXO(num);
            if(table.checkWin() == true){
                showTable();
                table.isWin();
                gameOver = true;
                saveStat();
                showStatPlayer();
                if(resetStat().equalsIgnoreCase("Y")){
                    reStat();
                    showStatPlayer();
                }
            }
            if(turnCount == 9){
                showTable();
                table.checkDraw();
                gameOver = true;
                saveStat();
                showStatPlayer();
                if(resetStat().equalsIgnoreCase("Y")){
                    reStat();
                    showStatPlayer();
                }
            }
            if(gameOver == true) {
                if (startNewgame().equalsIgnoreCase("Y")) {
                    newGame();
                    chosenNumbers.clear();
                    showWelcome();
                    continue;
                }else{
                break;
            }
            }
            table.switchPlayer();
            turnCount++;
        }
    }
    private void showWelcome(){
        System.out.println("Welcome to XO Game");
    } 
    private void showTable(){
        String[][] t = table.getTable();
        for(int i = 0 ; i < 3 ; i++){
            for(int j = 0 ; j < 3 ; j++){
                System.out.print(t[i][j]+" ");
            }
            System.out.println("");
        }
    }
    public void newGame(){
        table = new Table(Player1,Player2);
    }
    private int inputNumber(){
        Scanner kb = new Scanner(System.in);
        System.out.print("Please input your Number:");
        num = kb.nextInt();
        if(chosenNumbers.contains(num)) {
            showTable();
            showTurn();
            inputNumber();
        }
        chosenNumbers.add(num);
        return num;
    }

    private void showTurn() {
        System.out.println(table.getCurrentPlayer().getSymbol()+" Turn");
    }
    
    private String startNewgame(){
        System.out.print("Do you want to play again? (Y/N): ");
        Scanner kb = new Scanner(System.in);
        String playAgain = kb.nextLine();
        return playAgain;
    }
    private void saveStat(){
        if(table.getCurrentPlayer().getSymbol() == "X"){
            Player1.win();
            Player2.lose();
        }else if(table.getCurrentPlayer().getSymbol() == "O"){
            Player2.win();
            Player1.lose();
        }else{
            Player1.draw();
            Player2.draw();
        }
    }
    private void showStatPlayer(){
        System.out.println("-------------------------------------------");
        System.out.print("X Win Point: "+Player1.getWinCount()+" / ");
        System.out.print("X Lose Point: "+Player1.getLoseCount()+" / ");
        System.out.print("X Draw Point: "+Player1.getDrawCount());
        System.out.println("");
        System.out.print("O Win Point: "+Player2.getWinCount()+" / ");
        System.out.print("O Lose Point: "+Player2.getLoseCount()+" / ");
        System.out.println("O Draw Point: "+Player2.getDrawCount());
        System.out.println("-------------------------------------------");
    }
    
    private String resetStat(){
        System.out.println("Want to Reset Stats?(Y/N):");
        Scanner kb = new Scanner(System.in);
        String resetStat = kb.nextLine();
        return resetStat;
    }
    
    private void reStat(){
        Player1.resetWin();
        Player1.resetLose();
        Player1.resetDraw();
        Player2.resetWin();
        Player2.resetLose();
        Player2.resetDraw();
    }
}
